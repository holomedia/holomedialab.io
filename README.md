# holoMedia

holoMedia is a the channel where we publish multimedia [holoContent][1]

* holoTutorials,
* [holoGuides](https://holomedia.gitlab.io/hologuides),
* [holoBooks](https://holomedia.gitlag.io/holobooks)

## HoloContent (multimedia)

[1]: https://holomedia.gitlab.io/holocontent

HoloContent is a composition of [holoAssets][2] rendered for public use.
videos, slides, Maps, Graphs, PDFs documents and presentations. 

* page: [holoContent](https://holomedia.gitlab.io/holocontent)
* repository: [git](https://gitlab.com/holomedia/holocontent)
* wiki

## holoAssets 

[2]: https://holomedia.gitlab.io/holoassets


Holoassets are components for making holoContent such as images, clips etc. 

* page: [holoAssets](holomedia.gitlab.io/holoassets)
* repository: [git](https://gitlab.com/holomedia/holoassets)
* wiki

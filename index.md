---
---
# Welcome to holoMedia

The place where you'll learn everything from the [holoVerse][1]

our git repository group is : <https://gitlab.com/holoMedia>

our MLP page is at <https://holoteam.frama.io/mlp>

see the index at <https://holomedia.gitlab.io/holoIndex.htm>


[1]: https://duckduckgo.com/?q=!g+holoVerse+site:.gq+OR+site:.ml
